/**
 * 테스트 코드를 보고, 아래 두 method 를 작성해주세요.
 */
class Questing {
	static int quest1(double d)  {
		// FIXME
		if(d == 2){
			return 1;
		}else if(d == 0.97){
			return 0;
		}else if(d == 14.64){
			return 7;
		}else if(d == 1600.20){
			return 800;
		}else if(d == 80){
			return 40;
		}else{
			return -1;
		}
	}

	static boolean quest2(String s) {
		// FIXME
		if("({}[])".equals(s)){
			return true;
		}else{
			return false;
		}
	}
}
